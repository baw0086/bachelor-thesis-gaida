# Bachelor thesis Gaida

This code was written by me, Jan L. Gaida, in 2021 for my Bachelor's thesis "Brightness variations in cosmologically distant objects" at Hamburg University. I used python 3 and Jupyter Notebook.

I had next to no programming experience beforehand and the code was intended for personal use only, so I do not expect it will be useful or easily understandable for anyone. Most of the things I programmed will be possible to do much faster and more elegantly. I have not cleaned up the code, programmers will likely label it atrocious.

Nevertheless, my code is Creative Commons, so use it if you want, at your own peril.
